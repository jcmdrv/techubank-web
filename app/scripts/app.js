(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'account-info': '/account-info',
      'account-transfer': '/account-transfer',
      'account-movements': '/account-movements',
      'global-position': '/global-position',
      'login': '/login',
      'movement-detail': '/movement-detail',
      'movement-search': '/movement-search',   
      'new-transfer': '/new-transfer',   
      'user-settings': '/user-settings',
      'signup': '/signup',
      'home': '/',
      'another': '/another'
    }
  });

}(document));
