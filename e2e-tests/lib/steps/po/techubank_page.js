// login.page.js
const BasePage = require('./base_page')


let TechubankPage = Object.create(BasePage, {

  access: {
    value: function () {
      browser.url(`${this.config.urls[this.config.env]}`)
      browser.pause(1000)
    }
  },

  login: {
    value: function () {
      console.log('login')

      browser.setValue('login-page cells-template-paper-drawer-panel::div cells-login-view cells-credentials-form cells-molecule-input[id="user-input-id"] iron-input::input', 'purito')
      browser.setValue('login-page cells-template-paper-drawer-panel::div cells-login-view cells-credentials-form cells-molecule-input[id="user-input-pwd"] iron-input::input', 'secret')

      console.log(browser.getText('login-page cells-template-paper-drawer-panel::div cells-login-view cells-credentials-form cells-st-button::button#loginBtn'))
      browser.click('login-page cells-template-paper-drawer-panel::div cells-login-view cells-credentials-form cells-st-button::button#loginBtn')

      browser.pause(5000)
    }
  },

  checkGlobalPosition: {
    value: function () {
      browser.element('global-position-page')
    }
  }
})

/* 
await this.browser
.waitUntil(() => this.browser.isVisible('cells-bottom-modal'))
.execute(() => {
document
// TODO: Improve selector
.querySelector('cells-modal-alert:last-child')
.shadowRoot
.querySelector('cells-bottom-modal')
.close();
}) */

/* async function getExtShadowRoot(browser, CSS_SHADOW_HOST) {
  let shadowHost;
  await (shadowHost = browser.element(CSS_SHADOW_HOST));
  return browser.selectorExecute("return arguments[0].shadowRoot", shadowHost);
}

async function findShadowDomElement(browser, shadowDomElement) {
  let shadowRoot;
  let element;

  await (shadowRoot = getExtShadowRoot(browser, '#ironInput'));
  await shadowRoot.then(async (result) => {
    console.log('llego al root')
    await (element = result.element(shadowDomElement));
    console.log(element)
  });
  
  return element;
} */



module.exports = TechubankPage;
