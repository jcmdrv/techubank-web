const {
  config
} = require('../../../envs.conf'), {
  expect
} = require('chai');



function BasePage() {
  this.config = config;
}


BasePage.prototype.launchApp = function (url = config.urls[config.env]) {
  browser.url(url)
  browser.pause(2000);
}




module.exports = new BasePage()