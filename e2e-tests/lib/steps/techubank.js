const {
  Given,
  When,
  Then
} = require('cucumber'), {
  expect
} = require('chai'), {
  config
} = require('../../envs.conf'),
  TechubankPage = require('./po/techubank_page');

  

When(/^login successfully$/, {
  timeout: 60 * 1000
}, function () {
  TechubankPage.login()
})


Then(/^you are redirected to global position$/, {
  timeout: 60 * 1000
}, function () {
  TechubankPage.checkGlobalPosition()
})