
let os = require('os')
let default_config = {
    env: 'dev',
    cucumber_tags: '',
    user: '',
    key: '', 
    urls: {
        dev: 'http://localhost:8001/index.html',               
        docker: 'http://localhost:8001/index.html',
        kubernetes: 'http://localhost:8001/index.html'
    },
    report_config: {
        jsonDir: '',
        reportPath: '',
        metadata: {
            device: os.hostname(),
            platform: {
                name: os.platform(),
                version: os.release()
            }
        },
        customData: {
            title: 'Run info',
            data: [{
                label: 'Project',
                value: 'Techubank'
            },
            {
                label: 'Execution date',
                value: new Date().toLocaleString()
            }
            ]
        }
    }
}

/**
 * Overwrite default configuration with command line parameters
 *
 * Help: --node1.node2.node3 = new_value
 * {
 * 	node1: {
 * 		node2: {
 * 			node3: "old_value"
 * 		}
 * 	}
 * }
 * @param {*} config
 */
function overwrite_default_config(config) {
    process.argv.forEach((val, index) => {
        if ((val.substr(0, 2) === '--') && (val.indexOf('cucumberOpts') == -1)){
            const variable = val.substr(2),
                variable_array = variable.split('.')

            if (variable_array) {
                let tmp = config
                for (let i = 0; i < variable_array.length; i++) {
                        if (i === variable_array.length - 1) {
                            tmp[variable_array[i]] = process.argv[index + 1]
                        } else {
                            tmp = tmp[variable_array[i]]
                        }
                }
            }
        } 
    })
    return config
}

module.exports.config = overwrite_default_config(default_config)

