/* global describe it */
const assert = require('assert'),
	moment = require('../components/moment/moment')

describe('Work with dates', function () {
    it('A young kid cannot register, under 18 years old', function (done) {

			if (moment().subtract(18, 'years').isBefore(moment().subtract(8, 'years'))){
				assert(true)
			} else {
				assert(false)
			}
			done()
		})

    it('An adult person can register, over 18 yers old', function (done) {

			if (moment().subtract(18, 'years').isBefore(moment('05/01/1954', "DD/MM/YYYY"))){
				assert(false)
			} else {
				assert(true)
			}

			done()
		})
				
})
