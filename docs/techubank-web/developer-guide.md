# Introduction 
Techubank web is a Cells application mainly developed for mobile applications.

In this document we are going to explain the project structure and the different components reused.

# Project's folders
* app
	* **config**: config files for all the environments we have considered.
	* **locales-app**: json files with key-value pairs containing all application's texts.
	* **pages**: Components developed for this application
	* **resources**: Static files serve from the application
	* **scripts/app.js**: The entry points to the application (navigation)
	* **styles**
	* **tpls**: Templates used when building the app
* **bundle**: Helper used when building the Docker container
* **components**: Components downloaded via bower.
* **dist**: Folder whe the app:build command is executed
* **docs**: All markdown files
* **e2e-tests**: End To End Test project with only one feature (login). It's relevant to mention that this project is able to resolve the Shadow Dom issue (a big pain when testing with webcomponents)

# Application layout

## Unauthenticated pages

Open pages are:
* home-page: redirects to login-page
* login-page
* signup-page

These pages fills the full viewport and don't have header or footer.

## Authenticated pages

Authenticated pages follow the same layout:
* **Header**: with a minimalist menu showing your nave and avatar and the title of the page you are.
* **Body**: with the logic
* **Footer**: containing some actions (global-position, personal information, exit)

Inventory of components implemented:
* account-info-page: Show account detail information
* account-movements-page: List account movements
* account-transfer-page: Implements the transfer flow (fill the form, validation, confirm the operation, check the result)
* global-position-page: Once the user is logged is redirected to global-position, where the system shows all user's accounts.
* techubank-header-page: The header contains the logic for managing alerts (sending alerts using events) and the title where you are
* techubank-footer-page: Menu for navigate to global-position, personal information or exit the application
* techubank-weather-page: Component used in global-position. It gets weather information from your location.
* user-settings-page: Update your personal information

## Cells components reused 

* cells-basic-login/cells-basic-login
* cells-component-app-header/cells-component-app-header
* cells-copy-button/cells-copy-button
* cells-date-input/cells-date-input
* cells-demo-lang-switcher/cells-demo-lang-switcher
* cells-from-to/cells-from-to
* cells-generic-dp/cells-generic-dp
* cells-generic-dp/cells-generic-dp
* cells-i18n-behavior/cells-i18n-behavior
* cells-i18n-behavior/cells-i18n-behavior
* cells-login-view/cells-login-view
* cells-molecule-alert-slide/cells-molecule-alert-slide
* cells-molecule-input/cells-molecule-input
* cells-movements-list-item/cells-movements-list-item
* cells-navigation-bar/cells-navigation-bar
* cells-operations-list/cells-operations-list
* cells-organism-menu-sidebar/cells-organism-menu-sidebar
* cells-page/cells-page
* cells-page/cells-page
* cells-product-item-list/cells-product-item-list
* cells-product-more-info/cells-product-more-info
* cells-successful-response/cells-successful-response
* cells-summary-view/cells-summary-view
* cells-template-paper-drawer-panel/cells-template-paper-drawer-panel
* cells-template-zones/cells-template-zones

## Other components reused

* banking-icons/banking-icons
* coronita-icons/coronita-icons
* coronita-icons/coronita-icons
* moment/moment.js
* techubank-header-page/techubank-header-page
* techubank-weather-page/techubank-weather-page

# Security
Authentication and authorization is delegated to our OAuth2 Server. If login is correct the server will response with a token which will be stored in **sessionStorage**.

Each request to our backend must add the **authentication header**.

The header checks the token on the **ready** life cycle state and redirects to login page if it's not found.

Each request to the backend will validate if the token is valid.

We are using a JWT token, and the token is persisted in MongoDB too.

Communication should be over HTTPS in production, but for this practices it can run in both protocols. 

# Communication between components
We have used two ways of communication:

## Events
The header adds the component **cells-molecule-alert-slide**. This component is listening for the event *alert-message*.

In the example below we throws and error alert, wait 1500 miliseconds and redirecto to the Global Position.

```javascript
_getAccountInfoErrorResponse(data) {
	const alert = {
	type: 'error',
	text: this.t(data.detail.internal_code),
	_icon: 'coronita:error',
	timeout: 1500,
	noCancelOnOutsideClick: true,
	isStatic: false
	}

	this.publish('alert-message', alert)
	setTimeout(() => this.navigate('global-position'), 1500)
}
```

## sessionStorage
Sometimes it's difficult to orchestate events, because you throw the event before the other side starts to listen. In this cases we have decided to use the **sessionStorage**.

The use case is when you are in global position and select one accout to operate.

Example:
```javascript
transfer_tmp.from_iban = window.sessionStorage.getItem('account_iban_selected')
	this.set('transfer_info', transfer_tmp)

```

# Form Validations
We are cheking inputs in forms several times:

* We the user is typing in the form, the validator is executed at each event.
* When you click on the button we validate again the form and block the action if it is not correct.
* Finally, the backed will validate again the inputs. We are managing the model in the backend with Mongoose with let us the power to control the schema of the documents.

# Error propagation
Another relevant aspect is how we propagate errors to the user. Whe the backend throws an error it returns an Error Message object with an easy structure.
* internal_code: This code is mapped with our properties file with all the key-value pairs. It allows us to show the messsage in your language
* message: This message contains detail information about what really happens

Right now we are using a short list of internal-codes, many times generic messages, but don't worry, we are printing in the browser's console the detailed message.

# References

* [Cells](./cells.md)

# E2E testing
Testing should be a must in every project. In the scope of this project we have prepare a testing project scaffold y basic scenarios.

It should be neccessary to develop more scenarios aiming to validate the system as a hold, the most critical features.

E2E testing demands a lot of capacity, so it's recomended to balance the piramid to unit testing; F.I.R.S.T Principles of Unit Testing (FAST, ISOLATED/INDEPENDENT, REPEATABLE, SELF-VALIDATING and THOROUGH/TIMELY).

For further information about how to run E2E testing project and how to develop more scenarios, [click on the link](./e2e-tests.md).

# How to deploy a new version
Depending your environment you will build the application with a different configuration file.

## Localhost
We don't build the application, just serve it.
```
$ cells app:server -c dev.json
```

It serves the application in http://localhost:8081/index.html by default.

## Localhost - docker-compose
```
$ cells app:build -c docker.json
```
This is the only pipeline we haven't automated. You have to publish the docker image.
```
$ docker build -t davidrova/techubank-web:docker .          
$ docker pull davidrova/techubank-web  
```

It serves the application in http://localhost:8081/techubank/index.html by default.


## Cloud - Google Kubernetes
```
$ cells app:build -c kubernetes.json
```

This is the only pipeline we haven't automated. You have to publish the docker image.
```
$ docker build -t davidrova/techubank-web:kubernetes .          
$ docker pull davidrova/techubank-web  
```

It serves the application in http://<check-in-google-cloud-console>/techubank/index.html by default.


## AWS S3
```
$ cells app:build -c s3.json
```

Upload files to the bucket.

It serves the application in http://<check-in-aws-s3-console>/techubank/index.html by default.
