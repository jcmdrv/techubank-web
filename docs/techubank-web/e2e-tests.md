# Introduction

E2E testing project for Techubank application. 

It is important to inform something risky in this kind of projects. Develop web-components is smart, and allow us to be more productive while we are developing features. But, in terms of e2e testing, it adds a level of complexity, due to webcomponents.

There is a clear explanation about this issue in this [article](https://medium.com/rate-engineering/a-guide-to-working-with-shadow-dom-using-selenium-b124992559f).

![Shadow Dom](https://cdn-images-1.medium.com/max/2000/0*xE6N3pTQVoTo_k8R.png)

There are some utilities helping us to carry on this issue:
- [wdio-shadow](https://github.com/diego-fernandez-santos/wdio-shadow/blob/master/commands.js)
- [wdio-webcomponents](https://www.npmjs.com/package/wdio-webcomponents)


# Run tests

1)  Install dependencies
```
$   > cd e2e
$e2e> npm install
```
2) Run the tests
```
$e2e> ./node_modules/.bin/wdio ./wdio.conf.js --env dev --cucumber_tags '@techubank' 
```

There is an issue right now in wdio; if you want to filter tags using command line (you can overwrite the property in the config file wdio.conf.js)


## See the report
The report will be stored in "report" folder.

# Understanding the folders

- envs.conf.js: Different environment settings and default environment
- wdio.conf.js: WebdriverIO settings
- features: resides all Gherkin files
- steps: Where all Gherkin steps regular expressions will be implemented (in different folders and files)
- steps/po: Where all PageObject implementations will reside
- report: by default the report will be store here


# What is running under a e2e tests
In order to execute automated functional tests you need a Selenium Server running on your local machine or link to a Selenium Server hub.

There're three choices:
- Let wdio launches Selenium Server for you (used in this project using the selenium-standalone and chromedriver services)
- Using a local Selenium Server installation: recommended for debugging
- Using a docker container: recommended for Jenkins
- Link to an external Selenium Server HUB

With a Selenium Server running you can start to run your tests.

This project is set *forcing synchronous calls to the browser*. By default, any testing framework using JS execute commands in asynchronous mode.

More info: http://webdriver.io/guide/testrunner/gettingstarted.html


## Using a local Selenium Server installation

*Download latest selenium standalone server*
```
$ curl -O http://selenium-release.storage.googleapis.com/3.5/selenium-server-standalone-3.14.0.jar
```

*Download the latest version geckodriver for your environment and unpack it in your project directory*
Linux 64 bit
```
$ curl -L https://github.com/mozilla/geckodriver/releases/download/v0.16.0/geckodriver-v0.22.0-linux64.tar.gz | tar xz
```

*Download ChromeDriver*
```
$ curl -L https://chromedriver.storage.googleapis.com/index.html?path=2.42/
```

Include the ChromeDriver location in your PATH environment variable
```
$ export PATH=~/selenium:$PATH
```

*Start selenium standalone server*
```
$ java -jar -Dwebdriver.gecko.driver=./geckodriver selenium-server-standalone-3.14.0.jar
```
