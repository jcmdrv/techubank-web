# Requirements
Your host must be setup with the following software:

* Git
* Nodejs 8.4+
    * npm:  Globaldevtools Artifactory repository
        * In our case we have install cells-cli and change the registry to npmjs.org because we are publishing artifacts there
* Bower (only Cells requirement): Globaldevtools Artifactory repository
* Java SDK 1.8 (only Cells requirement)
* Python 2.7 (only Cells requirement)
* [cells-cli](https://platform.bbva.com/codelabs/cells/Cells%20Codelabs/#/cells/Prepare%20your%20Cells%20Environment/Overview/) 
* Docker 17+ if you want to publish a new docker image or run the latest TechU Bank docker image
* Docker-compose 1.17.x+
* Create a new MongoDB instance in (Mlab)[https://mlab.com/]
    * Create a *users* collection and add the following document (admin user _techubank_ with password _techubank1234_)

```json
{
    "scopes": [
        "admin", "user"
    ],
    "username": "techubank",
    "name": "techubank",
    "first_name": "techubank",
    "last_name": "techubank",
    "nif": "12345676y",
    "birth_date": "01/02/2019",
    "phone": "+34911234567",
    "email": "techubank@techu.com",
    "password": "f54c40c02cc861d619ad7d89d9|1a2490bd8f45b812da03ed47022e83a7|9c6c7617ed2b47cd4bf6be516663e99ae577d5dad9436a0941c04a6d1ff4aa8f",
    "address": {
        "street": "techubank",
        "zip_code": "techubank",
        "city": "techubank",
        "country": "techubank"
    },
    "register_date": {
        "$date": "2018-11-21T21:06:00.264Z"
    },
    "status": "ACTIVE"
}
```


# TechuBank environment variables

## Environment variables for COMPONENTS
Environment variables are fixed and loaded depending the **ENV** environment variable (local, docker, kubernetes, pro)

By default:
```json
exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'http',
    techubank_web_url: 'http://localhost:8001',    
    techubank_api_url: 'http://localhost:3000',
    techubank_mlab_url: 'http://localhost:5000',
    techubank_auth_server_url: 'http://localhost:4000'
}
```

### Set of available environments

Available environments:
* dev: localhost
* docker: docker-compose with all the pieces
* kubernetes: The whole application deployed in the Google Cloud Kubernetes
* s3: This component (static files) will be served from AWS S3 and the backend will be deployed in Google Cloud Kubernetes.
    * Backend IPs may change, so pay attention to this file


#### dev or local
You can access any component via *http* or *https*, but by default we are going to use *http*.

```
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'http',
    techubank_api_url: 'http://localhost:3000',
    techubank_mlab_url: 'http://localhost:5000',
    techubank_auth_server_url: 'http://localhost:4000'
```

#### docker
```
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'http',
    techubank_api_url: 'http://techubank-api:3000',
    techubank_mlab_url: 'http://techubank-mlab:5000',
    techubank_auth_server_url: 'http://techubank-auth-server:4000'
```

#### kubernetes
```
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'https',
    techubank_api_url: 'https://techubank-api-service:3443',
    techubank_mlab_url: 'https://techubank-mlab-service:5443',
    techubank_auth_server_url: 'https://techubank-auth-server-service:4443'
```


## Environment variables for WEB application
Variables below are stored in **app/config/{environment}.json**

* **techubank_api_url** (ej. https://localhost:3443)
* **techubank_auth_server_url** (ej. https://localhost:4443)

```json
{
  "cells_properties": {
    "deployEndpoint": "",
    "i18nPath": "locales/",
    "componentsPath": "./components/",
    "templatesPath": "./composerMocks/",
    "pages": [ "account-info", "account-movements", "account-transfer", "global-position", "login", "movement-detail", "movement-search", "new-transfer", "signup", "user-settings", "home", "another"],
    "pagesPath": "./pages/",
    "appId": "",
    "debug": true,
    "mocks": true,
    "coreCache": true,
    "routerLog": false,
    "cordovaScript" : "cordova.js",
    "prplLevel": 1,
    "initialBundle": ["login-page.html"],
    "transpile": true,
    "transpileExclude": [ "cells-rxjs", "webcomponentsjs", "moment","d3", "bgadp*" ]
  },
  "app_properties": {
    "techubank_api_url": "http://localhost:3443",
    "techubank_auth_server_url": "http://localhost:4443"
  }
}
```

## Environment variables for backend components
It is forbidden to store any kind of credential in your repository. A secured way to pass your credentials to the components is via environment variables.

First, let's explain the set of variables TechUBank manages.

* **ENV**: Choose the enviornment to use (dev, local, docker, kubernetes, pro). When using docker-compose you have to use *docker* value
* **MLAB_USERNAME**: Username with grants in [mlab mongodb](https://mlab.com/databases/techubank-jcmdrv)
* **MLAB_PASSWORD**: Mlab user's password
* **MLAB_HOST**: Host ant port to Mlab MongoDB
* **MLAB_DATABASENAME**: Mongo Database name
* **API_USERNAME**: Username with *admin* grants in *users* collection
```json
{
    "scopes": [
        "admin"
    ],
    "username": "techubank",
    "name": "techubank",
    "first_name": "techubank",
    "last_name": "techubank",
    "nif": "12345676y",
    "birth_date": "01/02/2019",
    "phone": "+34911234567",
    "email": "d.d@d.es",
    "password": "****************************",
    "address": {
        "street": "techubank",
        "zip_code": "techubank",
        "city": "techubank",
        "country": "techubank"
    },
    "register_date": {
        "$date": "2018-11-21T21:06:00.264Z"
    },
    "status": "ACTIVE"
}
```
* **API_PASSWORD**: Admin user's password

A subset of these variables (it depends the component) must be stored in a **.env** file on the basepath where you launch the node process. 

* [**techubank-api**](https://bitbucket.org/jcmdrv/techubank-api/src/): API Rest server consumed by techubank-web
    * *API_USERNAME*
    * *API_PASSWORD*
* [**techubank-auth-server**](https://bitbucket.org/jcmdrv/techubank-auth-server/src/): OAuth2 server
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
* [**techubank-mlab**](https://bitbucket.org/jcmdrv/techubank-mlab/src/): Isolate layer for storing objects into MongoDB. It encapsulates the different operations TechuBank wants to open over an API Rest.
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
    * *API_USERNAME*
    * *API_PASSWORD*
* [**techubank-compose**](https://bitbucket.org/jcmdrv/techubank-compose/src/): Orchestrate all the components in the launching process.
    * *ENV*: **docker**  
    * *MLAB_USERNAME*
    * *MLAB_PASSWORD*
    * *MLAB_HOST*
    * *MLAB_DATABASENAME*
    * *API_USERNAME*
    * *API_PASSWORD*


## HTTPS settings
Because this is a project for study purposes we have enabled both protocols (http and https).

It's neccessary to setup your certificates when using https; we have been working with a **[self-signed certificate](#Setup-https)**.

>
> When you work with a self-signed certificate web applications will block requests on first try, waiting for your reactive action > to accept the self-signed certificate.
>

If you want to serve only one of the protocols just modify in **index.js** the following lines:
```js
// CHECK IF COMMUNICATION IS OVER HTTP OR HTTPS
if (environment.value.protocol === 'https') {
    const https = require('https'),
        helmet = require('helmet')

    app.use(helmet()) // Add Helmet as a middleware
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 // this allows self-signed cer
    https.createServer({
        key: fs.readFileSync('key.pem'),
        cert: fs.readFileSync('certificate.pem')
    }, app).listen(port_ssl)
} else {
    app.listen(port)
}
```

### Setup https
Create an self-signed SSL certificate

* Write down the Common Name (CN) for your SSL Certificate. The CN is the fully qualified name for the system that uses the certificate. If you are using Dynamic DNS, your CN should have a wild-card, for example: *.api.com. Otherwise, use the hostname or IP address set in your Gateway Cluster (for example. 192.16.183.131 or dp1.acme.com).

* Run the following OpenSSL command to generate your private key and public certificate. Answer the questions and enter the Common Name when prompted.

```bash
$ openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem

Country Name (2 letter code) [AU]:localhost
string is too long, it needs to be no more than 2 bytes long
Country Name (2 letter code) [AU]:ES
State or Province Name (full name) [Some-State]:MADRID
Locality Name (eg, city) []:MADRID
Organization Name (eg, company) [Internet Widgits Pty Ltd]:techubank
Organizational Unit Name (eg, section) []:techubank
Common Name (e.g. server FQDN or YOUR name) []:techubank
Email Address []:techubank@techu.com
```

* key.pem is the private key
* certificate.pem is the self-signed certificate
* Copy the pem files on the basepath 

```bash
$ techubank-auth-server > ls -lrt
drwxrwxr-x   2 techu techu    4096 oct 16 10:43 bin
-rw-rw-r-*   1 techu techu     180 oct 19 08:28 Dockerfile
-rw-r--r-*   1 techu techu     680 oct 30 10:32 sonar-scanner.properties
drwxrwxr-x   2 techu techu    4096 oct 30 16:24 test
-rw-rw-r-*   1 techu techu    2396 nov  6 09:26 bitbucket-pipelines.yml
drwxrwxr-x   3 techu techu    4096 nov 12 07:59 src
-rw-rw-r-*   1 techu techu    1633 nov 12 07:59 package.json
drwxrwxr-x   2 techu techu    4096 nov 12 07:59 ws-doc
-rw------*   1 techu techu    1708 nov 12 09:50 key.pem
-rw-rw-r-*   1 techu techu    1436 nov 12 09:51 certificate.pem
drwxrwxr-x 523 techu techu   20480 nov 12 09:53 node_modules
-rw-rw-r-*   1 techu techu    6483 nov 12 09:54 README.md

```

References: 
* [How to use ssl/tls with Node](https://www.sitepoint.com/how-to-use-ssltls-with-node-js/)
* [Gernerate self signed openSSL](https://www.ibm.com/support/knowledgecenter/en/SSWHYP_4.0.0/com.ibm.apimgmt.cmc.doc/task_apionprem_gernerate_self_signed_openSSL.html)


# Launch the application
You have different choices:
* Localhost
    * [Launching each piece with npm commands](#Localhost---developer)
    * [Using docker-compose](#Localhost---docker-compose)
    * [Using docker](#Localhost---docker)
* [Cloud](#Cloud)

## Localhost

### For developers

1) Launch each peace of the architecture in this order: 
    * techubank-mlab
    * techubank-auth-server
    * techubank-api
```bash
$ npm install
$ npm run start
```
Everything will be fine if you can read
```bash
> techubank-xxx@1.0.24 start /git/techubank/techubank-xxx
> nodemon src/index.js

[nodemon] 1.18.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node src/index.js`
info: ************************
info: Logger initialized
info: ************************
info: Properties from environment.XXXX loaded
info: App listening HTTP on port 5000
info: App listening HTTPS on port 5443

```

2) Launch the web: techubank-web
```bash
// install bower dependencies
$ bower install

// launch Cells over one of the available environments
$ cells app:serve -c dev.json  (other environment choices are: kubernetes, docker)
```

Everything will be fine if you can read
```bash
[mainline] Access URLs:
 --------------------------------------------
    Local: http://localhost:8001/index.html
 External: http://10.23.43.47:8001/index.html
 --------------------------------------------
[mainline] Serving files from: dist
[2018-11-28 11:33:21] [INFO] ◉ finished step [launch]
[2018-11-28 11:33:21] [INFO] ◎ starting step [watch]
[2018-11-28 11:33:21] [INFO] ◉ finished step [watch]

```

3) [Launch the app](http://localhost:8001/index.html)


### docker-compose
1) Clone [**techubank-compose**](https://bitbucket.org/jcmdrv/techubank-compose) project.
```bash
$ git clone git@bitbucket.org:jcmdrv/techubank-compose.git
$ cd techubank-compose
```

2) Setup environment variables values in a *.env* file located at docker-compose.yaml level
```properties
ENV=docker
MLAB_USERNAME=<mlab_username>
MLAB_PASSWORD=<mlab_password>
MLAB_HOST=<mlab_host>
MLAB_DATABASENAME=<mlab_databasename>
API_USERNAME=<admin_username>
API_PASSWORD=<admin_password>
```

3) Setup your certificate (key.pem and certificate.pem)
Our componentes are going to be launched listening both protocols (http and htttps), so you need to setup propperly at least a self-signed certificate; [how to create a self-signed certificate](#HTTPS-settings)

Copy key.pem and certificate.pem on the basepath.
```bash
$ techubank-auth-server > ls -lrt
drwxrwxr-x   2 techu techu    4096 oct 16 10:43 bin
-rw-rw-r-*   1 techu techu     180 oct 19 08:28 Dockerfile
-rw-r--r-*   1 techu techu     680 oct 30 10:32 sonar-scanner.properties
drwxrwxr-x   2 techu techu    4096 oct 30 16:24 test
-rw-rw-r-*   1 techu techu    2396 nov  6 09:26 bitbucket-pipelines.yml
drwxrwxr-x   3 techu techu    4096 nov 12 07:59 src
-rw-rw-r-*   1 techu techu    1633 nov 12 07:59 package.json
drwxrwxr-x   2 techu techu    4096 nov 12 07:59 ws-doc
-rw------*   1 techu techu    1708 nov 12 09:50 key.pem
-rw-rw-r-*   1 techu techu    1436 nov 12 09:51 certificate.pem
drwxrwxr-x 523 techu techu   20480 nov 12 09:53 node_modules
-rw-rw-r-*   1 techu techu    6483 nov 12 09:54 README.md

```

4) Be sure to fetch latest containers version
```
$ docker-compose pull
```

5) Launch the ecosystem
```
$ docker-compose up
```

6) [Open TechuBank WEB](http://localhost:8001/techubank/index.html)

If you want to develop only the front, it could be interesting for you to customize **docker-compose.yml** and laucn the backend only.

### docker

1) Setup your certificate (key.pem and certificate.pem)
Our componentes are going to be launched listening both protocols (http and htttps), so you need to setup propperly at least a self-signed certificate; [how to create a self-signed certificate](#HTTPS-settings)

Copy key.pem and certificate.pem on the basepath.
```bash
$ techubank-auth-server > ls -lrt
drwxrwxr-x   2 techu techu    4096 oct 16 10:43 bin
-rw-rw-r-*   1 techu techu     180 oct 19 08:28 Dockerfile
-rw-r--r-*   1 techu techu     680 oct 30 10:32 sonar-scanner.properties
drwxrwxr-x   2 techu techu    4096 oct 30 16:24 test
-rw-rw-r-*   1 techu techu    2396 nov  6 09:26 bitbucket-pipelines.yml
drwxrwxr-x   3 techu techu    4096 nov 12 07:59 src
-rw-rw-r-*   1 techu techu    1633 nov 12 07:59 package.json
drwxrwxr-x   2 techu techu    4096 nov 12 07:59 ws-doc
-rw------*   1 techu techu    1708 nov 12 09:50 key.pem
-rw-rw-r-*   1 techu techu    1436 nov 12 09:51 certificate.pem
drwxrwxr-x 523 techu techu   20480 nov 12 09:53 node_modules
-rw-rw-r-*   1 techu techu    6483 nov 12 09:54 README.md

```

2) techubank-mlab
```bash
$ docker run -it -p 5000:5000 -p 5443:5443 -e PORT=5000 -e PORT_SSL=5443 -e MLAB_HOST=<mlab-host>:<mlab_port> -e MLAB_DATABASENAME=<database> -e MLAB_USERNAME=<mlab-username> -e MLAB_PASSWORD=<mlab-password> -e ENV=docker davidrova/techubank-mlab
```

3) techubank-auth-server
```
$ docker run -it -p 4000:4000 -p 4443:4443 -e PORT=4000 -e PORT_SSL=4443 -e MLAB_HOST=<mlab-host>:<mlab_port> -e MLAB_DATABASENAME=<database> -e MLAB_USERNAME=<mlab-username> -e MLAB_PASSWORD=<mlab-password> -e ENV=docker davidrova/techubank-auth-server
```

4) techubank-api
```
$ docker run -it -p 3000:3000 -p 3443:3443 -e PORT=5000 -e PORT_SSL=5443 -e MLAB_HOST=<mlab-host>:<mlab_port> -e MLAB_DATABASENAME=<database> -e MLAB_USERNAME=<mlab-username> -e MLAB_PASSWORD=<mlab-password> -e ENV=docker -e API_USERNAME=<admin-user> -e API_PASSWORD=<admin-password> davidrova/techubank-api
```

5) techubank-web
```
$ docker run -it -p 8001:8001  -p 8443:8443 -e PORT=8001 -e PORT_SSL=8443 -e API_USERNAME=<admin-user> -e API_PASSWORD=<admin-password> -e ENV=docker davidrova/techubank-web
```

## Cloud
We have configured:
* The backend en Google Kubernetes
* Frontend (statics) in AWS S3

[Termporal link to the demo](https://s3-eu-west-1.amazonaws.com/test-techu/index.html#!/login)

>
> It is a https connection with a untrusted certify. You have to accept the untrusted certify if you want
> to operate with the app
>

Further information about how we have deployed the app in the [following link](./cloud.md).

