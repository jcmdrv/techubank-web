# Architecture
![Architecture](https://bitbucket.org/jcmdrv/techubank-web/raw/94c549ff96824583e950c02b812f266977a72214/docs/images/TechU-Bank-Architecture.png)


Source code repositories list:

* [techubank-web](https://bitbucket.org/jcmdrv/techubank-web/src/): Cells application
* [techubank-auth-server](https://bitbucket.org/jcmdrv/techubank-auth-server/src/): OAuth2 server
* [techubank-api](https://bitbucket.org/jcmdrv/techubank-api/src/): API Rest server consumed by techubank-web
* [techubank-mlab](https://bitbucket.org/jcmdrv/techubank-mlab/src/): Isolated layer for storing objects into MongoDB. It encapsulates the different operations TechuBank wants to open over an API Rest.
* [techubank-commons](https://bitbucket.org/jcmdrv/techubank-commons/src/): Common functions from several scopes: environment variables, crypt, security...

## Communitation
All the components communicate each other over HTTP/HTTPS.

## Security
All the components are authenticated with OAuth2.

## External services
We are consuming an external API Rest service, authenticated using API-KEY.

If you need further information check each component's repository.