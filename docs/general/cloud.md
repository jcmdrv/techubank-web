# Deploy Techubank in the cloud

![Docker-Architecture](https://bitbucket.org/jcmdrv/techubank-web/raw/94c549ff96824583e950c02b812f266977a72214/docs/images/TechU-Bank-Docker-Architecture.png)

We have configured:
* The backend en Google Kubernetes
* Frontend (statics) in AWS S3
    * [Termporal link to the demo](https://s3-eu-west-1.amazonaws.com/test-techu/index.html#!/login)

>
> It is a https connection with a untrusted certify. You have to accept the untrusted certify if you want
> to operate with the app
>

## Deploy the frontend
This is the easiest part, but should be done after the backend is deployed if you are not working with DNS names.

1) Create an S3 Bucket
2) Upload the content in **techubank-web/dist** folder after building the app with **s3** profile
```
$ cells app:build -c s3.json
```

3) All the content must be public (read access)
4) Finally copy the link Amazon provides you to access your S3 Bucket

>
> If you own a domain you can configure Amazon DNS service in order to resolve properly the url.
>

## Deploy the backend
We have tested Google Cloud Kubernetes, besides OpenShift. The target is the same but the steps to follow not.

Steps in a nutshell:
1) Create a new Kubernete cluster: A Kubernetes cluster is a managed group of uniform VM instances for running Kubernetes. 
2) Create a **workload** for each Techubank's component: Workloads are deployable units of computing that can be created and managed in a cluster.
    * Container image: techubank-*:kubernetes (**it's important to notice ver container version used**)
    * [Environment variables](./lauch-techubank.md#Environment-variables-for-backend-components)
    * Application name: techubank-*
    * Select the cluster created in first step
    * Deploy

>
> This process will create autocatically a pod with a container of each **workload** created
>

3) Create a **service** for each **workload**. Services are sets of pods with a network endpoint that can be used for discovery and load balancing. Ingresses are collections of rules for routing external HTTP(S) traffic to services.
    * From the workload detail you will see a link to create the service
    * Specify the ports origin and target
    

With these steps you will be able to access the containers from internet. For instance:
```
techubank-api-service	
    35.240.48.24:3000 
    35.240.48.24:3443 
```

Each **workload** has a configuration YAML associated too.

* **Workload** has all your parameters defined
* **Config** has the values to your parameters

>
> You cand modify your workloads, adding parameters,... 
>

## Update my pods (images, variables)
If you modify your image, change a parameter value, ... you will have to **restart the pod**.

Follow these steps:
* Click on your **workload**
* Click on your **Managed pods**
* Click on **Delete**

The cluster identifies the pod has been deleted and will create a new one, updating parameters and image.


## How my containers communicate each other

Relevant information when working with kubernetes is how the communication between containers will be resolved.

In Google Cloud Kubernetes services' names are standarized with the sufix '-service'. Our backend_url settings remains in this way:


```json
// file: techubank-commons/src/environment/environment.kubernetes.js
exports.environment = {
    production: false,
    branch: 'develop',
    enableCors: true,
    protocol: 'https',
    techubank_web_url: 'https://techubank-web-service:8443',    
    techubank_api_url: 'https://techubank-api-service:3443',
    techubank_mlab_url: 'https://techubank-mlab-service:5443',
    techubank_auth_server_url: 'https://techubank-auth-server-service:4443'
}

```

>
> Each time a pod is destroyed is going to make a pull of a new image before starting again.
>

