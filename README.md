# Introduction
Techubank is a financial web application used for study purposes.

We have focused our effort in a short list of features, trying to apply some DevOps practices, without a visual impact but a long term benefit.

## DevOps good practices carried on
* **Coding**
	* Code review via Pull Request
	* Semver
	* Static code analysis (eslint, Sonarqube)
	* API documentation with Swagger 2
* **Build & Deploy**
	* Automated pipeline using Bitbucket pipelines instead Jenkins pipeline
	* Publish artifacts when pushing to **develop** or **master** (containers in Docker Hub, components in npmjs)
	* Enable local development dockerifying components
* **Testing**
	* Unit testing
	* Integration testing
	* E2E testing
* **Infrastructure**
	* Deploy manually the application over AWS (statics over S3) and Google Kubernetes (backend)

## How to Play with TechU Bank
There are several ways to launch de application. Check [**developer guide**](docs/general/launch-techubank.md) for further details.

Launch the app, test it and think about improving the project.


# Features
Techu Bank allows you to sign up and operate with a dummy bank.

## Basic features
* **Sign up users**: when you sign up into the app we create an account with 100€ for you
* **Login management**: Login into the app and logout. 
	* Authentication and authorization is managed using OAuth2. It is supported with [**techubank-auth-server**](https://bitbucket.org/jcmdrv/techubank-auth-server/src).
	* Login will create an object in your **sessionStorage** with the JWT token. That token is used to make requests to [**techubank-api**]((https://bitbucket.org/jcmdrv/techubank-api/src))
* **List movements**: List account's movements
* **Create movements**: Make a transfer between two accounts
* **Request external APIs**
	* TechU Bank consumes **openweathermap.org** in global position to improve user experience, giving some dynamism to the app. This feature will allow us to customize look & feel depending the weather in the feature.
	* It gathers weather info related to the location (if it is enabled)
	* **openweathermap.org**:
		* https://openweathermap.org/appid
		* https://openweathermap.org/current


## Aditional features

* **Security**
	* Apply OAuth2 security to all the pieces.
		* [Understanding OAuth2 and Building a Basic Authorization Server of Your Own: A Beginner’s Guide](https://medium.com/google-cloud/understanding-oauth2-and-building-a-basic-authorization-server-of-your-own-a-beginners-guide-cf7451a16f66) 
	* Control sessions via [JWT tokens](https://jwt.io/introduction/)
* *Account info*: show detailed information about de account
* **Enable multilanguage**: internalization i18n

# Architecture overview

![Architecture](https://bitbucket.org/jcmdrv/techubank-web/raw/94c549ff96824583e950c02b812f266977a72214/docs/images/TechU-Bank-Architecture.png)


Source code repositories list:

* [techubank-web](https://bitbucket.org/jcmdrv/techubank-web/src/): Cells application
* [techubank-auth-server](https://bitbucket.org/jcmdrv/techubank-auth-server/src/): OAuth2 server
* [techubank-api](https://bitbucket.org/jcmdrv/techubank-api/src/): API Rest server consumed by techubank-web
* [techubank-mlab](https://bitbucket.org/jcmdrv/techubank-mlab/src/): Isolated layer for storing objects into MongoDB. It encapsulates the different operations TechuBank wants to open over an API Rest.
* [techubank-commons](https://bitbucket.org/jcmdrv/techubank-commons/src/): Common functions from several scopes: environment variables, crypt, security...
* [techubank-compose](https://bitbucket.org/jcmdrv/techubank-compose/src/): Orchestrate all the components in the launching process.
* [sonar-scaner](https://bitbucket.org/jcmdrv/sonar-scanner/src/master/): Container with sonar-scaner; it will allow us to execute static code analysis over the project and publish the result in the public Sonarqube server.

Further information about all the pieces in the architecture section [click on the link](docs/general/architecture.md)

# Related documents

* [Architecture detail](docs/general/architecture.md)
* [Launch Techubank](docs/general/launch-techubank.md)
* **Developer guides**
	* [Techubank web developer guide](docs/techubank-web/developer-guide.md)
	* [Techubank api developer guide](https://bitbucket.org/jcmdrv/techubank-api/src/master/README.md)
	* [Techubank auth-server developer guide](https://bitbucket.org/jcmdrv/techubank-auth-server/src/master/README.md)
	* [Techubank mlab developer guide](https://bitbucket.org/jcmdrv/techubank-mlab/src/master/README.md)
	* [Techubank commons developer guide](https://bitbucket.org/jcmdrv/techubank-commons/src/master/README.md)
* [User guide](docs/general/user-guide.md)

