FROM node:8.12.0-alpine 

ARG PORT=8001
ARG PORT_SSL=8443
ARG ENV='kubernetes'


ENV PORT=$PORT
ENV ENV=$ENV

WORKDIR /techubank-web
RUN mkdir /techubank-web/dist
ADD ./dist /techubank-web/dist
ADD ./bundle /techubank-web

COPY /bundle/index.html /techubank-web/dist/index.html

EXPOSE $PORT
EXPOSE $PORT_SSL

RUN npm install

CMD ["npm", "run", "start"]

